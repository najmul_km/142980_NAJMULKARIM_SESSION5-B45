<?php

// Bolean example started from here
$decision = true;
if($decision) echo "true";
echo "<br>";


$decision = false;
if($decision) echo "false";


// Bolean example ended from here



//int value  started from here
$value = 100;
echo "$value". "<br>";
//int value ended here


//float value  started from here
$value = 35.56;
echo "$value". "<br>";
//float value ended here


//string example from here

$var = 100;
$string1 = 'This is a double quoted string $var <br>';
$string2 = "This is a double quoted string $var <br> ";

echo $string1.$string2 ;
echo "<br>";

//string example end here

$heredocString=<<<BITM
      this is heredoc example line1 $var <br>
      this is heredoc example line2 $var <br>
      this is heredoc example line3 $var <br>
      this is heredoc example line4 $var <br>
      this is heredoc example line5 $var <br>
BITM;


$nowdocString=<<<'BITM'


      this is nowdoc example line1 $var <br>
      this is nowdoc example line2 $var <br>
      this is nowdoc example line3 $var <br>
      this is nowdoc example line4 $var <br>
      this is nowdoc example line5 $var <br>

BITM;

echo $heredocString . "<br> <br>" .$nowdocString;
//end


//array

$arr = array (1,2,3,4,5,6,7,8,9,10);
print_r($arr);
echo "<br>";


$arr = array("BMW", "Toyota","Nissan","Tata");
print_r($arr);
echo"<br>";

var_dump($arr);
echo "<br>";


$ageArray = array("Arif"=>30,"Moynar ma"=>45,"Rahim"=>25);
echo " The age of Moynar ma is ".$ageArray["Moynar ma"];
//print_r($ageArray);
echo"<br>";
?

